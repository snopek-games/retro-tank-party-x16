.ifndef UTIL_INC
UTIL_INC = 1

.macro inc_16bit addr
	clc
	lda addr
	adc #1
	sta addr
	lda addr+1
	adc #0
	sta addr+1
.endmacro

.macro dec_16bit addr
	sec
	lda addr
	sbc #1
	sta addr
	lda addr+1
	sbc #0
	sta addr+1
.endmacro

.macro lsr_16bit addr
    lda addr+1
    lsr
    sta addr+1
    lda addr
    ror
    sta addr
.endmacro

.macro lsr_16bit_n addr, n
    ldx #n
:   lsr_16bit addr
    dex
    bne :-
.endmacro

.endif