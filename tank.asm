.ifndef TANK_INC
TANK_INC = 1

.include "vram.asm"

SPRITE_NO_FLIP = $00
SPRITE_H_FLIP = $01
SPRITE_V_FLIP = $02
SPRITE_HV_FLIP = $03
SPRITE_VH_FLIP = SPRITE_HV_FLIP

; These are just the high byte of the position
; >> 2, because we need to << 6 for fixed point, and then >> 8 to get only the high byte.
SPRITE_MAX_X = (640 - 32) >> 2
SPRITE_MIN_X = 0
SPRITE_MAX_Y = (480 - 32) >> 2
SPRITE_MIN_Y = 0
; a dividing line to tell the difference between going off the left/top vs right/bottom side
; it's only the high byte of a 10 bit number
SPRITE_WRAP_VALUE = $C0

;
; Look-up tables
;

sprite_orientation_addr_table:
	; Address offest >> 5 (to be or'd into the base address)
	.byte $00 ; up
	.byte $10 ; up-right
	.byte $20 ; right
	.byte $10 ; down-right
	.byte $00 ; down
	.byte $10 ; down-left
	.byte $20 ; left
	.byte $10 ; up-left

sprite_orientation_flip_table:
	; Sprite v-flip/h-flip
	.byte SPRITE_NO_FLIP ; up
	.byte SPRITE_NO_FLIP ; up-right
	.byte SPRITE_NO_FLIP ; right
	.byte SPRITE_V_FLIP ; down-right
	.byte SPRITE_V_FLIP ; down
	.byte SPRITE_VH_FLIP ; down-left
	.byte SPRITE_H_FLIP ; left
	.byte SPRITE_H_FLIP ; up-left

	; Normalized forward movement vector for each orientation.
sprite_orientation_vector_x_table:
	; 120 pixels per second
	.word $0000, $002d, $0040, $002d, $0000, $ffd3, $ffc0, $ffd3
sprite_orientation_vector_y_table:
	; 120 pixels per second
	.word $ffc0, $ffd3, $0000, $002d, $0040, $002d, $0000, $ffd3

;
; Variables
;

; Current joy state.
joy_state: .res 3
; Previous joy state.
joy_last: .byte 0

; Fixed point numbers, with 6 bits used for the fractional part.
sprite_x: .word 0
sprite_y: .word 0

; 3 bit number (0-7) represention orientation where 0 is up and it increases clockwise.
sprite_orientation: .byte 0

; 0 = not moving
; 1 = forward
; 2 = backward
sprite_moving: .byte 0

; Number of jiffies left or right have been held.
sprite_turning_held: .byte 0
; Number of jiffies until holding counts as a turn.
HELD_TURN_JIFFIES = 20

tank_start:
	stz joy_state
	stz joy_state+1
	stz joy_state+2
	lda #$FF
	sta joy_last

	stz sprite_turning_held

	; initialize sprite position
	lda #<(32 << 6)
	sta sprite_x
	lda #>(32 << 6)
	sta sprite_x+1
	lda #<(32 << 6)
	sta sprite_y
	lda #>(32 << 6)
	sta sprite_y+1

	stz sprite_orientation
	stz sprite_moving

	; initialize sprite attributes
	VERA_SET_ADDR VRAM_SPRITE1_sprattr, 1
	lda #(VRAM_sprites >> 5) & $FF
	sta VERA_data0
	lda #(VRAM_sprites >> 13) ; upper 4 bits of address, and 4bpp mode
	sta VERA_data0
	lda sprite_x
	sta VERA_data0
	lda sprite_x+1
	sta VERA_data0
	lda sprite_y
	sta VERA_data0
	lda sprite_y+1
	sta VERA_data0
	lda #(3 << 2) ; z-depth of 3, in front of layer 1
	sta VERA_data0
	lda #($A0 | SPRITE_PALETTE_OFFSET) ; 32x32 with palette offset
	sta VERA_data0

    rts

tank_process:
	jsr tank_handle_input
	jsr tank_update_position
	jsr tank_update_sprite_attributes
	rts

tank_handle_input:
	lda #0
	jsr JOYSTICK_GET
	sta joy_state
	stx joy_state+1
	sty joy_state+2
	; When first starting the emulator, the joystate is always $00 until something is pressed,
	; we just return right away if we're in that state.
	cmp #0
	bne @check_left
	rts
	; Check left/right to adjust orientation
@check_left:
	bit #$02
	bne @check_right
	lda joy_last
	bit #$02
	bne @left
	lda sprite_turning_held
	inc
	cmp #HELD_TURN_JIFFIES
	bpl @left
	sta sprite_turning_held
	bra @check_forward
@left:
	stz sprite_turning_held
	lda sprite_orientation
	dec
	bpl @store_orientation
	lda #7
	bra @store_orientation
@check_right:
	bit #$01
	bne @check_forward
	lda joy_last
	bit #$01
	bne @right
	lda sprite_turning_held
	inc
	cmp #HELD_TURN_JIFFIES
	bpl @right
	sta sprite_turning_held
	bra @check_forward
@right:
	stz sprite_turning_held
	lda sprite_orientation
	inc
	cmp #8
	bne @store_orientation
	lda #0
@store_orientation:
	sta sprite_orientation
	; Check forward/backward to adjust 'sprite_moving'
@check_forward:
	lda joy_state
	bit #$08
	beq @forward
	bit #$04
	beq @backward
	lda #0
	bra @store_moving
@backward:
	lda #2
	bra @store_moving
@forward:
	lda #1
@store_moving:
	sta sprite_moving
@return:
	lda joy_state
	sta joy_last
	rts

tank_update_position:
	; update position
	lda sprite_moving
	bne @check_backward
	rts
@check_backward:
	cmp #1
	bne @backward
@forward:
	; double the sprite orientation to get the index into the vector table
	lda sprite_orientation
	asl
	tax

	; add vector x
	clc
	lda sprite_x
	adc sprite_orientation_vector_x_table,x
	sta sprite_x
	inx
	lda sprite_x+1
	adc sprite_orientation_vector_x_table,x
	sta sprite_x+1
	dex

	; add vector y
	clc
	lda sprite_y
	adc sprite_orientation_vector_y_table,x
	sta sprite_y
	inx
	lda sprite_y+1
	adc sprite_orientation_vector_y_table,x
	sta sprite_y+1

	bra @check_bounds
@backward:
	; double the sprite orientation to get the index into the vector table
	lda sprite_orientation
	asl
	tax

	; subtract vector x
	sec
	lda sprite_x
	sbc sprite_orientation_vector_x_table,x
	sta sprite_x
	inx
	lda sprite_x+1
	sbc sprite_orientation_vector_x_table,x
	sta sprite_x+1
	dex

	; subtract vector y
	sec
	lda sprite_y
	sbc sprite_orientation_vector_y_table,x
	sta sprite_y
	inx
	lda sprite_y+1
	sbc sprite_orientation_vector_y_table,x
	sta sprite_y+1

	; Clamp tank position within screen
@check_bounds:
@check_bounds_x:
	lda sprite_x+1
	cmp #SPRITE_MAX_X
	bcc @check_bounds_y
	cmp #SPRITE_WRAP_VALUE
	bcs @sprite_x_to_zero
	lda #SPRITE_MAX_X
	sta sprite_x+1
	stz sprite_x
	bra @check_bounds_y
@sprite_x_to_zero:
	stz sprite_x+1
	stz sprite_x
@check_bounds_y:
	lda sprite_y+1
	cmp #SPRITE_MAX_Y
	bcc @return
	cmp #SPRITE_WRAP_VALUE
	bcs @sprite_y_to_zero
	lda #SPRITE_MAX_Y
	sta sprite_y+1
	stz sprite_y
	bra @return
@sprite_y_to_zero:
	stz sprite_y+1
	stz sprite_y
@return:
	rts

tank_update_sprite_attributes:
	VERA_SET_ADDR VRAM_SPRITE1_sprattr, 1
	; Set sprite address
	ldy sprite_orientation
	lda sprite_orientation_addr_table,y
	ora #((VRAM_sprites >> 5) & $FF) ; add to the lower byte of the base address
	sta VERA_data0

	; Can we skip this just by reading the value?
	inc VERA_addr_low

	; Shift the sprite x position 6 to the right and set in sprite attributes.
	lda sprite_x+1
	lsr
	sta ZP_TMP+1
	lda sprite_x
	ror
	sta ZP_TMP
	lsr_16bit_n ZP_TMP, 5
	sta VERA_data0 ; the low byte is still in the accumulator
	lda ZP_TMP+1
	sta VERA_data0

	; Now do the same for y.
	lda sprite_y+1
	lsr
	sta ZP_TMP+1
	lda sprite_y
	ror
	sta ZP_TMP
	lsr_16bit_n ZP_TMP, 5
	sta VERA_data0 ; the low byte is still in the accumulator
	lda ZP_TMP+1
	sta VERA_data0

	; Set the sprite flip bits.
	lda sprite_orientation_flip_table,y
	ora #(3 << 2) ; z-depth of 3, in front of layer 1
	sta VERA_data0

	rts


.endif