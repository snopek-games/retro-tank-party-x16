#!/bin/bash

set -euo pipefail

./convert-image.py -t 16x16 -P TILESP.BIN tiles.png TILES.BIN 
./convert-image.py -t 32x32 -P SPRITEP.BIN sprite.png SPRITE.BIN 

./convert-tilemap.py map.json MAP.BIN -p 2

cl65 -t cx16 -l rtp.list -o RTP.PRG main.asm


