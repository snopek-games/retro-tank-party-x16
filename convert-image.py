#!/usr/bin/env python3

import sys
import argparse
from PIL import Image

class Palette(object):
    def __init__(self, maximum):
        self.colors = [(0, 0, 0)]
        self.max = maximum

    def add_or_lookup(self, color):
        if color[3] != 0xFF:
            return 0

        # Remove lower nibble, converting to a 12-bit color
        color = (color[0] & 0xF0, color[1] & 0xF0, color[2] & 0xF0)

        for index, existing in enumerate(self.colors):
            if color == existing:
                return index

        if len(self.colors) == self.max:
            raise Exception("Can't have more than %s colors in palette" % self.max)

        self.colors.append(color)

        return len(self.colors) - 1

    def to_binary(self):
        output = bytearray([0, 0])
        for color in self.colors:
            output.append(color[1] | color[2] >> 4)
            output.append(color[0] >> 4)
        return output

def main():
    parser = argparse.ArgumentParser(description='Convert tilesets from images to binary data ready to be loaded into VRAM on the Commander x16')
    parser.add_argument('input_filename', help='The image file to convert')
    parser.add_argument('output_filename', help='The binary file to write')
    parser.add_argument('--generate-palette', '-P', metavar="FILENAME", help='Write a binary palette file (number of colors depends on bpp)')
    parser.add_argument('--tiled', '-t', metavar="WIDTHxHEIGHT", help='Put into memory as a collection of tiles')
    # @todo Add a 'tile-direction' argument, so the can be started vertically if that makes sense sometimes
    args = parser.parse_args()

    im = Image.open(args.input_filename)
    im = im.convert("RGBA")

    if args.tiled:
        # @todo validate this argument
        (tile_width, tile_height) = tuple(map(lambda x: int(x), args.tiled.split('x')))
    else:
        tile_width = im.width
        tile_height = im.height

    # @todo Check that height/width are a multiple of tile_height/width
    col_max = int(im.width / tile_width)
    row_max = int(im.height / tile_height)

    # @todo take palette as input
    # @todo have the ability to generate a palette (4, 16 or 256 colors)
    # @todo palette size should be configurable
    palette = Palette(16)

    # Start with empty header
    output = bytearray([0, 0])

    for row in range(row_max):
        base_y = row * tile_height
        for col in range(col_max):
            base_x = col * tile_width
            for y in range(tile_height):
                last_index = None
                for x in range(tile_width):
                    color = im.getpixel((base_x + x, base_y + y))
                    index = palette.add_or_lookup(color)

                    # @todo configurable bit depths (this is just 4bpp)
                    if last_index is not None:
                        output.append((last_index << 4) | index)
                        last_index = None
                    else:
                        last_index = index

    outf = open(args.output_filename.upper(), "wb")
    outf.write(output)
    outf.close()

    if args.generate_palette:
        outp = open(args.generate_palette, 'wb')
        outp.write(palette.to_binary())
        outp.close()

if __name__ == "__main__": main()

