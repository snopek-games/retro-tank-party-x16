#!/bin/bash

set -euo pipefail

TILED=${TILED:-tiled}

$TILED --export-map json map.tmx map.json

