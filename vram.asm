.ifndef VRAM_INC
VRAM_INC = 1

;
; This file is responsible to for loading all our data into VRAM, and
; defining all the constants necessary to access that data.
;

.include "x16.inc"

.macro load_file_into_vram fn, fn_length, vram_addr
	lda #1
	ldx #8
	ldy #0 ; ignore the address in the file header
	jsr SETLFS
	lda #fn_length
	ldx #<fn
	ldy #>fn
	jsr SETNAM
	lda #(^vram_addr + 2) ; VRAM bank + 2
	ldx #<vram_addr
	ldy #>vram_addr
	jsr LOAD
.endmacro

; VRAM addresses
VRAM_layer0_map = $04000
VRAM_tiles = $08000

TILES_PALETTE_OFFSET = 2
VRAM_tiles_palette = VRAM_palette + (32 * TILES_PALETTE_OFFSET)

SPRITE_PALETTE_OFFSET = 3
VRAM_sprite_palette = VRAM_palette + (32 * SPRITE_PALETTE_OFFSET)

VRAM_sprites = $0B000
SPRITE_SIZE = 512 ; 32x32 @ 4bpp

; The tanks
VRAM_SPRITE1_sprattr = VRAM_sprattr + (8 * 1)
VRAM_SPRITE2_sprattr = VRAM_sprattr + (8 * 2)
VRAM_SPRITE3_sprattr = VRAM_sprattr + (8 * 3)
VRAM_SPRITE4_sprattr = VRAM_sprattr + (8 * 4)
; The bullets
VRAM_SPRITE5_sprattr = VRAM_sprattr + (8 * 5)
VRAM_SPRITE6_sprattr = VRAM_sprattr + (8 * 6)
VRAM_SPRITE7_sprattr = VRAM_sprattr + (8 * 7)
VRAM_SPRITE8_sprattr = VRAM_sprattr + (8 * 8)
VRAM_SPRITE9_sprattr = VRAM_sprattr + (8 * 9)
VRAM_SPRITE10_sprattr = VRAM_sprattr + (8 * 10)
VRAM_SPRITE11_sprattr = VRAM_sprattr + (8 * 11)
VRAM_SPRITE12_sprattr = VRAM_sprattr + (8 * 12)
VRAM_SPRITE13_sprattr = VRAM_sprattr + (8 * 13)
VRAM_SPRITE14_sprattr = VRAM_sprattr + (8 * 14)
VRAM_SPRITE15_sprattr = VRAM_sprattr + (8 * 15)
VRAM_SPRITE16_sprattr = VRAM_sprattr + (8 * 16)
; The crates and powerups
VRAM_SPRITE17_sprattr = VRAM_sprattr + (8 * 17)
VRAM_SPRITE18_sprattr = VRAM_sprattr + (8 * 18)
; Where the "map object" sprites start
VRAM_MAP_SPRITE_BASE_sprattr = VRAM_sprattr + (8 * 19)

;
; Variables
;

tiles_fn: .byte "tiles.bin"
end_tiles_fn:

tiles_palette_fn: .byte "tilesp.bin"
end_tiles_palette_fn:

sprite_fn: .byte "sprite.bin"
end_sprite_fn:

sprite_palette_fn: .byte "spritep.bin"
end_sprite_palette_fn:

map_fn: .byte "map.bin"
end_map_fn:

;
; Sub-routines
;

vram_start:
	; We load the test data first, so we know if we failed (we'll see the test data instead of garbage)
	jsr load_test_tiles
	jsr load_tiles
	jsr load_test_map
	jsr load_map

	; Load some sprites
	;jsr load_test_sprite
	jsr load_sprite

    rts

load_tiles:
	; Load the tiles into VRAM
	load_file_into_vram tiles_fn, (end_tiles_fn-tiles_fn), VRAM_tiles
	load_file_into_vram tiles_palette_fn, (end_tiles_palette_fn-tiles_palette_fn), VRAM_tiles_palette
	rts

load_test_tiles:
	VERA_SET_ADDR VRAM_tiles, 1

	; Fill each tile with 1 color for the 16 color palette
	ldx #0
	ldy #0
:	sty ZP_TMP
	tya
	asl
	asl
	asl
	asl
	ora ZP_TMP
	sta VERA_data0
	inx
	cpx #128
	bne :-
	ldx #0
	iny
	cpy #16
	bne :-

	rts

load_map:
	; Load map into VRAM
	load_file_into_vram map_fn, (end_map_fn-map_fn), VRAM_layer0_map
	rts

load_test_map:
	VERA_SET_ADDR VRAM_layer0_map, 1

	; Write the first 16 tiles to the screen
	ldx #0
:	stx VERA_data0
	;lda #(TILES_PALETTE_OFFSET << 4)
	lda #0
	sta VERA_data0
	inx
	cpx #16
	bne :-

	rts

load_sprite:
	load_file_into_vram sprite_fn, (end_sprite_fn-sprite_fn), VRAM_sprites
	load_file_into_vram sprite_palette_fn, (end_sprite_palette_fn-sprite_palette_fn), VRAM_sprite_palette


	rts

.endif