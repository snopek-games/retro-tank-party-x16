.org $080d
.segment "STARTUP"
.segment "INIT"
.segment "ONCE"
.segment "CODE"

    jmp start

.include "x16.inc"
.include "util.inc"
.include "vram.asm"
.include "tank.asm"

; PETSCII
Q_CHAR = $51

; VERA
VSYNC_BIT = $01
DISPLAY_SCALE = 128 ; 1x scale

; Variables
default_irq_vector: .addr 0

start:
	lda #$51 ; Enable sprites, layer 0 and VGA video
	sta VERA_dc_video

	lda #DISPLAY_SCALE
	sta VERA_dc_hscale
	sta VERA_dc_vscale

	lda #$52 ; 64x64 map, with 4bpp
	sta VERA_L0_config

	lda #(VRAM_layer0_map >> 9)
	sta VERA_L0_mapbase
	lda #((VRAM_tiles >> 9) | $03)
	sta VERA_L0_tilebase

	; Is it necessary to reset these?
	stz VERA_L0_hscroll_l
	stz VERA_L0_hscroll_h
	stz VERA_L0_vscroll_l
	stz VERA_L0_hscroll_h

    jsr vram_start
    jsr tank_start

	lda IRQVec
	sta default_irq_vector
	lda IRQVec+1
	sta default_irq_vector+1

	sei
	lda #<custom_irq_handler
	sta IRQVec
	lda #>custom_irq_handler
	sta IRQVec+1
	lda #VSYNC_BIT
	sta VERA_ien
	cli
@main_loop:
	wai
	jsr GETIN
	cmp #Q_CHAR
	bne @main_loop
@exit:
	; Restore IRQ handler
	sei
	lda default_irq_vector
	sta IRQVec
	lda default_irq_vector+1
	sta IRQVec+1
	cli

	; Reset VERA and return to BASIC
	lda #$21
	sta VERA_dc_video
	rts

custom_irq_handler:
	lda VERA_isr
	and #VSYNC_BIT
	beq @return
    ; Game tick (aka "process")
	jsr tank_handle_input
	jsr tank_process
@return:
	jmp (default_irq_vector)
